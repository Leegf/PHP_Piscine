#!/usr/bin/php
<?php

function ft_split($str)
{
    $tmp = preg_replace('!\s+!', ' ', trim($str));
    $arr = explode(" ", $tmp);
    sort($arr);
    return ($arr);
}

function sum($arr)
{
	if (count($arr) != 2)
	{
		print("Syntax Error\n");
		exit;
	}
	if (!ctype_digit(trim($arr[0])) && !strstr(trim($arr[0]), ".") && !strstr(trim($arr[0]), "-") || (!ctype_digit(trim($arr[1])) && !strstr(trim($arr[1]), ".") && !strstr(trim($arr[1]),"-")))
	{
		print("Syntax Error\n");
		exit;
	}
	if (ctype_digit(trim($arr[0])) && ctype_digit(trim($arr[1])))
		print(trim($arr[0]) + trim($arr[1])."\n");
	else if (is_float(floatval(trim($arr[0]))) || is_float(floatval(trim($arr[1]))))
	{
		if (strstr(trim($arr[1]), " ") || strstr(trim($arr[0]), " "))
		{
			print("Syntax Error\n");
			exit;
		}
		print(trim($arr[0]) + trim($arr[1])."\n");
	}
	else
		print("Syntax Error\n");
}

function minus($arr)
{
	if (count($arr) != 2)
	{
		print("Syntax Error\n");
		exit;
	}
	if (!ctype_digit(trim($arr[0])) && !strstr(trim($arr[0]), ".") && !strstr(trim($arr[0]), "-") || (!ctype_digit(trim($arr[1])) && !strstr(trim($arr[1]), ".") && !strstr(trim($arr[1]),"-")))
	{
		print("Syntax Error\n");
		exit;
	}
	if (ctype_digit(trim($arr[0])) && ctype_digit(trim($arr[1])))
		print(trim($arr[0]) - trim($arr[1])."\n");
	else if (is_float(floatval(trim($arr[0]))) || is_float(floatval(trim($arr[1]))))
	{
		if (strstr(trim($arr[1]), " ") || strstr(trim($arr[0]), " "))

		{
			print("Syntax Error\n");
			exit;
		}
		print(intval(trim($arr[0])) - intval(trim($arr[1]))."\n");
	}
	else
		print("Syntax Error\n");
}

function multiply($arr)
{
	if (count($arr) != 2)
	{
		print("Syntax Error\n");
		exit;
	}
	if (!ctype_digit(trim($arr[0])) && !strstr(trim($arr[0]), ".") && !strstr(trim($arr[0]), "-") || (!ctype_digit(trim($arr[1])) && !strstr(trim($arr[1]), ".") && !strstr(trim($arr[1]),"-")))
	{
		print("Syntax Error\n");
		exit;
	}
	if (ctype_digit(trim($arr[0])) && ctype_digit(trim($arr[1])))
		print(trim($arr[0]) * trim($arr[1])."\n");
	else if (is_float(floatval(trim($arr[0]))) || is_float(floatval(trim($arr[1]))))
	{
		if (strstr(trim($arr[1]), " ") || strstr(trim($arr[0]), " "))

		{
			print("Syntax Error\n");
			exit;
		}
		print(trim($arr[0]) * trim($arr[1])."\n");
	}
	else
		print("Syntax Error\n");
}

function division($arr)
{
	if (count($arr) != 2)
	{
		print("Syntax Error\n");
		exit;
	}
	if (!ctype_digit(trim($arr[0])) && !strstr(trim($arr[0]), ".") && !strstr(trim($arr[0]), "-") || (!ctype_digit(trim($arr[1])) && !strstr(trim($arr[1]), ".") && !strstr(trim($arr[1]),"-")))
	{
		print("Syntax Error\n");
		exit;
	}
	if (ctype_digit(trim($arr[0])) && ctype_digit(trim($arr[1])))
	{
		if (trim($arr[1]) == "0")
			exit;
		print(trim($arr[0]) / trim($arr[1])."\n");
	}
	else if (is_float(floatval(trim($arr[0]))) || is_float(floatval(trim($arr[1]))))
	{
		if (strstr(trim($arr[1]), " ") || strstr(trim($arr[0]), " "))
		{
			print("Syntax Error\n");
			exit;
		}
		if (trim($arr[1]) == "0")
			exit;
		print(trim($arr[0]) / trim($arr[1])."\n");
	}
	else
		print("Syntax Error\n");
}

function modulo($arr)
{
	if (count($arr) != 2)
	{
		print("Syntax Error\n");
		exit;
	}
	if (!ctype_digit(trim($arr[0])) && !strstr(trim($arr[0]), ".") && !strstr(trim($arr[0]), "-") || (!ctype_digit(trim($arr[1])) && !strstr(trim($arr[1]), ".") && !strstr(trim($arr[1]),"-")))
	{
		print("Syntax Error\n");
		exit;
	}
	if (ctype_digit(trim($arr[0])) && ctype_digit(trim($arr[1])))
	{
		if (trim($arr[1]) == "0")
			exit;
		print(trim($arr[0]) % trim($arr[1])."\n");
	}
	else if (is_float(floatval(trim($arr[0]))) || is_float(floatval(trim($arr[1]))))
	{
		if (strstr(trim($arr[1]), " ") || strstr(trim($arr[0]), " "))
		{
			print("Syntax Error\n");
			exit;
		}
		if (trim($arr[1]) == "0")
			exit;
		print(trim($arr[0]) % trim($arr[1])."\n");
	}
	else
		print("Syntax Error\n");
}

if ($argc != 2)
	print("Incorrect Parameters\n");
else
{
	$tmp = trim($argv[1]);
	if (strstr($argv[1], "+"))
	{
		$arr = explode("+", $argv[1]);
		sum($arr);
	}
	else if (strstr($argv[1], "-"))
	{
		$arr = explode("-", $argv[1]);
		minus($arr);
	}
	else if (strstr($argv[1], "*"))
	{
		$arr = explode("*", $argv[1]);
		multiply($arr);
	}
	else if (strstr($argv[1], "/"))
	{
		$arr = explode("/", $argv[1]);
		division($arr);
	}
	else if (strstr($argv[1], "%"))
	{
		$arr = explode("%", $argv[1]);
		modulo($arr);
	}
	else
		print("Syntax Error\n");
}
?>
