#!/usr/bin/php
<?php
for (;;)
{
	print("Enter a number: ");
	$line = fgets(STDIN);
	if($line === FALSE)
		break;
	$num = intval($line);
	if ((strlen($line) - 1) != strlen("$num")) {
		$tmp = str_replace("\n", "", $line);
		printf("'%s' is not a number\n", $tmp);
		continue;
	}
	else if (!is_numeric(trim($line))) {
		$tmp = str_replace("\n", "", $line);
		printf("'%s' is not a number\n", $tmp);
		continue;
	}
	if (($num === 0) || ($num % 2 === 0)) {
		printf("The number %d is even\n", $num);
	}
	else {
		printf("The number %d is odd\n", $num);
	}
}
?>

