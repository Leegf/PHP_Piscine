#!/usr/bin/php
<?php

function print_arr($arr){
	for ($i = 0; $i < count($arr); $i++)
		print("$arr[$i]"."\n");
}

function	compare_everth($str1, $str2)
{
	for ($i = 0; $i < strlen($str1) && $i < strlen($str2); $i++)
	{
		while (substr($str1, $i, 1) == substr($str2, $i, 1))
			$i++;
		if (ctype_alpha(substr($str1, $i, 1)) && ctype_alpha(substr($str2, $i, 1)))
			return (strcmp(strtolower(substr($str1, $i, 1)), strtolower(substr($str2, $i, 1))));
		else if (ctype_digit(substr($str1, $i, 1)) && ctype_digit(substr($str2, $i, 1)))
			return (substr($str1, $i, 1) - substr($str2, $i, 1));
		else if (ctype_alpha(substr($str1, $i, 1)) && ctype_digit(substr($str2, $i, 1)))
			return (-1);
		else if (ctype_digit(substr($str1, $i, 1)) && ctype_alpha(substr($str2, $i, 1)))
			return (1);
		else if (ctype_alnum(substr($str1, $i, 1)) && !ctype_alnum(substr($str2, $i, 1)))
			return (-1);
		else if (!ctype_alnum(substr($str1, $i, 1)) && ctype_alnum(substr($str2, $i, 1)))
			return (1);
		else if (!ctype_alnum(substr($str1, $i, 1)) && !ctype_alnum(substr($str2, $i, 1)))
			return (strcmp(substr($str1, $i, 1), substr($str2, $i, 1)));
	}
		return (0);
}

function	sort_everything($arr)
{
	usort($arr, compare_everth);
	print_arr($arr);
}

if ($argc > 1) {
	$str = $argv[1];
	for ($i = 2; $i < $argc; $i++){
		$str.=' ';
		$str.=$argv[$i];
	}
		$tmp = preg_replace('!\s+!', ' ', trim($str));
		$arr = explode(" ", $tmp);
		sort_everything($arr);
}
?>
