#!/usr/bin/php
<?php

function print_arr($arr){
	for ($i = 0; $i < count($arr); $i++)
		print("$arr[$i]"."\n");
}

if ($argc > 1) {
	$str = $argv[1];
	for ($i = 2; $i < $argc; $i++){
		$str.=' ';
		$str.=$argv[$i];
	}
		$tmp = preg_replace('!\s+!', ' ', trim($str));
		$arr = explode(" ", $tmp);
		sort($arr);
		print_arr($arr);
}
?>
