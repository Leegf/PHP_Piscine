#!/usr/bin/php
<?php
function ft_split($str)
{
    $tmp = preg_replace('!\s+!', ' ', trim($str));
    $arr = explode(" ", $tmp);
    return ($arr);
}

function print_arr($arr){
	for ($i = 0; $i < count($arr) - 1; $i++)
		print("$arr[$i]"." ");
	print($arr[$i]."\n");
}

if ($argc > 1)
{
	$arr = ft_split($argv[1]);
	$tmp = $arr[0];
	array_shift($arr);
	array_push($arr, $tmp);
	print_arr($arr);
}

?>
