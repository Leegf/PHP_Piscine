#!/usr/bin/php
<?php
if ($argc != 4)
	print("Incorrect Parameters\n");
else
{
	switch(trim($argv[2])){
	case "+":
		print(trim($argv[1]) + trim($argv[3])."\n");
		break;
	case "-":
		print(trim($argv[1]) - trim($argv[3])."\n");
		break;
	case "*":
		print(trim($argv[1]) * trim($argv[3])."\n");
		break;
	case "/":
		if (trim($argv[3]) == "0")
			exit;
		print(trim($argv[1]) / trim($argv[3])."\n");
		break;
	case "%":
		if (trim($argv[3]) == "0")
			exit;
		print(trim($argv[1]) % trim($argv[3])."\n");
		break;
	default:
		break;
	}
}
?>
