<?php
if ($_POST["submit"] != "OK" || $_POST["oldpw"] == "" || $_POST["newpw"] == "" || $_POST["login"] == "") {
	exit("ERROR\n");
}

$flag = 0;
$arr2 = unserialize(file_get_contents("../private/passwd"));
foreach ($arr2 as $i => $val) {
	if ($val["login"] == $_POST["login"]) {
		if ($val["passwd"] == hash("whirlpool", $_POST["oldpw"])) {
			$arr2[$i]["passwd"] = hash("whirlpool", $_POST["newpw"]);
			$flag++;
			break;
		}
		else
			exit ("ERROR\n");
	}
}
if ($flag == 0)
	exit ("ERROR\n");
file_put_contents("../private/passwd", serialize($arr2));
echo "OK\n";
header("Location: index.html");
?>
