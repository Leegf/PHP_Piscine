<?php
session_start();
include ("auth.php");
if (!auth($_POST["login"], $_POST["passwd"])) {
	$_SESSION["loggued_on_user"] = "";
	echo "ERROR\n";
	exit;
}
else {
	$_SESSION["loggued_on_user"] = $_POST["login"];
	header("Content-type:text/html");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>42 chat</title>
    <style>
        iframe {
            width:100%;
            height:550px;
            overflow: hidden;
        }
        iframe[name='speak']{
            height:50px;
        }
    </style>
</head>
<body>
<iframe name='chat' src='chat.php'></iframe>
<iframe name='speak' src='speak.php'></iframe>
</body>
</html>
