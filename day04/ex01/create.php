<?php

if ($_POST["submit"] != "OK" || $_POST["passwd"] == "" || $_POST["login"] == "") {
	echo "ERROR\n";
	exit;
}
if (!file_exists("../private/passwd")) {
	mkdir("../private", 0755, true);
}
$arr2 = unserialize(file_get_contents("../private/passwd"));
foreach ($arr2 as $elem) {
	if ($elem["login"] == $_POST["login"]) {
		exit("ERROR\n");
	}
}
$arr2[] = ["login"=>$_POST["login"],"passwd"=>hash("whirlpool", $_POST["passwd"])];
file_put_contents("../private/passwd", serialize($arr2));
echo "OK\n";
?>
