<?php
class Lannister
{
	public function sleepWith($class_name)
	{
		if (get_parent_class($class_name) == "Lannister" && get_class($class_name) == "Cersei" && get_class($this) == "Jaime") {
			echo "With pleasure, but only in a tower in Winterfell, then." . PHP_EOL;
		}
		else if (get_class($this) == "Jaime" && get_class($class_name) == "Sansa") {
			echo "Let's do this." . PHP_EOL;
		}
		else if (get_class($this) == "Tyrion" && get_class($class_name) == "Sansa") {
			echo "Let's do this." . PHP_EOL;
		}
		else
			echo "Not even if I'm drunk !" . PHP_EOL;
	}
}
?>