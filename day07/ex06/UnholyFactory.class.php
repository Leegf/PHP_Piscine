<?php
class UnholyFactory
{
	private $_arr = array();

	public function absorb($type_of_soldier)
	{
		if (get_parent_class($type_of_soldier) != "Fighter") {
			echo "(Factory can't absorb this, it's not a fighter)\n";
		} else if (in_array($type_of_soldier, $this->_arr)) {
			print("(Factory already absorbed a fighter of type ");
			switch (get_class($type_of_soldier)) {
				case "Footsoldier":
					echo "foot soldier)" . PHP_EOL;
					break;
				case "Archer":
					echo "archer)" . PHP_EOL;
					break;
				case "Assassin":
					echo "assassin)" . PHP_EOL;
					break;
				default:
					break;
			}
		} else {
			echo "(Factory absorbed a fighter of type ";
			switch (get_class($type_of_soldier)) {
				case "Footsoldier":
					echo "foot soldier)" . PHP_EOL;
					break;
				case "Archer":
					echo "archer)" . PHP_EOL;
					break;
				case "Assassin":
					echo "assassin)" . PHP_EOL;
					break;
				default:
					break;
			}
			array_push($this->_arr, $type_of_soldier);
		}
	}
	public function fabricate($rf)
	{
			switch ($rf) {
				case "foot soldier":
					echo "(Factory fabricates a fighter of type foot soldier)\n";
					return (new Footsoldier());
					break;
				case "archer":
					echo "(Factory fabricates a fighter of type archer)\n";
					return (new Archer());
					break;
				case "assassin":
					echo "(Factory fabricates a fighter of type assassin)\n";
					return (new Assassin());
					break;
				default:
					echo "(Factory hasn't absorbed any fighter of type " . $rf .")\n";
					return (null);
					break;
			}
	}
}
?>