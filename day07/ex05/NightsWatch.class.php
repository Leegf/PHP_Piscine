<?php

class NightsWatch {
	private $_arr = array();
	public function recruit($class_name) {
		array_push($this->_arr, $class_name);
	}
	public function fight() {
		foreach ($this->_arr as $fighter)
		{
			if (method_exists($fighter, 'fight')) {
				$fighter->fight();
			}
		}
	}
}
?>
