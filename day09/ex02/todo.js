var count = 0;

check_cookie();
function check_cookie() {
    if (window.atob(getCookie('inp')))
        document.getElementById('ft_list').innerHTML = window.atob(getCookie('inp'));
}

function todo(){
    count = getCookie("count");
    var inp = prompt('Create a new todo:', '');
    if (!inp)
        return;
    var tmp = document.getElementById('ft_list').innerHTML;
    var hm = document.getElementById('ft_list').innerHTML = '<div id= "el' + count + '" onclick="delete_el(this.id);">' + inp + '</div>' + tmp;
    count++;
    setCookie('count', count, 20);
    setCookie('inp', window.btoa(String(hm)), 20);
}

function delete_el(id) {
    if (confirm('Are you sure want do delete it?')) {
        var elem = document.getElementById(id);
        elem.parentNode.removeChild(elem);
        var hm = document.getElementById('ft_list').innerHTML;
        deleteCookie('inp');
        setCookie('inp', window.btoa(String(hm)), 20);
    }
}

function deleteCookie(cname) {
    setCookie(cname, "", {
        expires: -1
    })
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = 'expires='+ d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(cname) {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
}