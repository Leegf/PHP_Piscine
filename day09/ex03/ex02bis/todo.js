var count = 0;
var flag = 0;

check_cookie();

function check_cookie() {
    var str = window.atob(getCookie('inp'));
    $(document).ready(function() {
        $('#ft_list').prepend(str);
    });
}

$(document).on("click", ".hm", function() {
    if (flag) {
        if (confirm('Are you sure want do delete it?')) {
            var id = "#" + $(this).attr('id');
            $(id).remove();
            str = ($('#ft_list').html());
            deleteCookie('inp');
            setCookie('inp', window.btoa(str), 20);
        }
    }
});

$(document).ready(function() {
    $('button').click(function () {
        count = getCookie("count");
        var inp = prompt('Create a new todo:', '');
        if (!inp)
            return;
        var str = '<div id= "el' + count + '" class="hm'+ '">' + inp + '</div>';
        $('#ft_list').prepend(str);
        str = ($('#ft_list').html());
        count++;
        setCookie('count', count, 20);
        setCookie('inp', window.btoa(str), 20);
        flag = 1;
    });
    $('.hm').click(function () {
        if (confirm('Are you sure want do delete it?')) {
            var id = "#" + $(this).attr('id');
            $(id).remove();
            str = ($('#ft_list').html());
            deleteCookie('inp');
            setCookie('inp', window.btoa(str), 20);
            flag = 0;
        }
    });
});

function deleteCookie(cname) {
    setCookie(cname, "", {
        expires: -1
    })
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = 'expires='+ d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(cname) {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
}