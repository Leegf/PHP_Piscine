<?php
session_start();
if ($_SESSION["flag"] == "true") {
    $_SESSION["from_cart"] = "true";
	header("location: buy.php");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XBookShop: categories</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body>
<div class="cont_2">
    <div class="head">
        <header><h1><a href="index.php">XBookShop</a></h1>
            <div class="search"></div>
            <form action="cart_buy.php" class="cart" method="POST">
                <button type="submit" value="OK">
                    <img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
                </button>
            </form>
            <form action="login.php" class="login" method="POST">
			    <?php
			    if ($_SESSION["loggued_on_user"]) {
				    echo '<button type="submit" value="OK" name="submit" disabled class="logout">' . 'Welcome, ' . $_SESSION['loggued_on_user'] . '
<ul class="additional_func hid_log">
<li class="elem">
<a href="logout.php">logout</a>
</li>';
				    if ($_SESSION["loggued_on_user"] != "admin")
					    echo '</ul>';
				    else
					    echo '
<li class="elem">
<a href="add_book.php">add book</a>
</li>
<li class="elem">
<a href="orders.php">manage orders</a>
</li>
<li class="elem">
<a href="users.php">manage users</a>
</li>
</ul>
 </button>';
			    }
			    else
				    echo '<button type="submit" value="OK" name="submit">login</button>';
			    ?>
            </form>
        </header>
    </div>
</div>
<div class="container">
    <aside class="side_category">
        <?php if (!$_SESSION["loggued_on_user"]) {
            echo '
	        <form action = "" id = "order" method = "post" >
            <strong id = "nonlogin" ><a href="login.php">LOGIN FIRST</a></strong >';
        }
        else {
        echo '
	        <form action = "validate_order.php" id = "order" method = "post" >
            <p><b>Make your order!</b></p>
            <div>
                <div class="wrap_for_check">
                    <span class="block">Your first name:</span>
                    <input type="text" placeholder="Vasya" name="first_name" required autofocus><br /><br/>
                </div>
                <div class="wrap_for_check">
                    <span class="block">Your last name:</span>
                    <input type="text" placeholder="Petrov" name="last_name" required><br /><br/>
                </div>
                <div class="wrap_for_check">
                    <span class="block">Your address:</span>
                    <textarea cols="39" rows="5" placeholder="USA, LA, Baba st., 43" name="adress" required ></textarea><br />
                </div>
                <button type="submit" value="OK" name="submit">
                    ORDER</button>
            </div>
        ';
        }?>
        </form>
    </aside>
    <div class="goods" id="order_goods">
        <strong>Your purchases:</strong>
        <?php
        include "db/cart.php";
        $books = get_cart_entries_by_user($_SESSION["loggued_on_user"]);
        foreach ($books as $b) {
        $id = $b['book_id'];
        ?>
        <div class="cart_good">
            <a href="<?php echo "book_page.php"."?id=$id" ?>" class="title_2"><?php echo $b['name']?></a>,
            <span class="price_2"><?php echo $b['price']?> $</span>
            <span class="xx"><a href=<?php echo "delete_item_from_cart.php"."?id=".$b['id']?>><img src="https://www.pngarts.com/files/1/X-Shape-Free-PNG-Image.png" alt="cross" height="15px" width="15px"></a></span>
            <span class="quant"><?php echo $b['quan']?> items</span>
        </div>
        <?php
        }
        ?>
        <div class="quantity">
            <?php
            foreach ($books as $b) {
                $total_cost += $b['price'] * $b['quan'];
            }
            ?>
            Total cost: <?php echo $total_cost?> $
        </div>
    </div>
</div>
</body>
</html>
