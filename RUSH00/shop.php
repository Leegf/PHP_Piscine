<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XBookShop: categories</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body>
<div class="cont_2">
<div class="head">
    <header><h1><a href="index.php">XBookShop</a></h1>
        <div class="search"></div>
        <form action="cart_buy.php" class="cart" method="POST">
            <button type="submit" value="OK">
                <img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
            </button>
        </form>
        <form action="login.php" class="login" method="POST">
			<?php
			if ($_SESSION["loggued_on_user"]) {
				echo '<button type="submit" value="OK" name="submit" disabled class="logout">' . 'Welcome, ' . $_SESSION['loggued_on_user'] . '
<ul class="additional_func hid_log">
<li class="elem">
<a href="logout.php">logout</a>
</li>';
				if ($_SESSION["loggued_on_user"] != "admin")
					echo '</ul>';
				else
					echo '
<li class="elem">
<a href="add_book.php">add book</a>
</li>
<li class="elem">
<a href="orders.php">manage orders</a>
</li>
<li class="elem">
<a href="users.php">manage users</a>
</li>
</ul>
 </button>';
			}
			else
				echo '<button type="submit" value="OK" name="submit">login</button>';
			?>
        </form>
    </header>
    </div>
    <div class="container">
    <aside class="side_category">
        <form action="shop.php" class="category" method="get">
            <p><b>Choose categories: </b></p>
            <div>
                <div class="wrap_for_check">
                <input type="checkbox" id = "1cat" name="horror" value="true">
                <label for="1cat">Horror</label>
                </div>
                <div class="wrap_for_check">
                <input type="checkbox" id = "2cat" name="thriller" value="true">
                <label for="2cat">Thriller</label>
                </div>
                <div class="wrap_for_check">
                <input type="checkbox" id = "3cat" name="fantasy" value="true">
                <label for="3cat">Fantasy</label>
                </div>
                <div class="wrap_for_check">
                <input type="checkbox" id = "4cat" name="science_fiction" value="true">
                <label for="4cat">Science Fiction</label>
                </div>
                <div class="wrap_for_check">
                <input type="checkbox" id = "5cat" name="light_novel" value="true">
                <label for="5cat">Light Novel</label>
                </div>
                <div class="wrap_for_check">
                <input type="checkbox" id = "6cat" name="novel" value="true">
                <label for="6cat">Novel</label>
                </div>
                <div class="wrap_for_check">
                <input type="checkbox" id = "7cat" name="detective" value="true">
                <label for="7cat" id="last_label">Detective</label>
                </div>
                <button type="submit" value="OK" name="submit">
                FILTER</button>
            </div>
        </form>
    </aside>
        <div class="goods">
            <?php
            include "db/book.php";
            $books = array();
            if ($_GET['submit'] != 'OK' || ($_GET['submit'] == 'OK' && count($_GET) == 1))
                $books = get_all_books();
            else {
                foreach ($_GET as $key => $val) {
                    $category_books = get_book_by_category($key);
                    $books = array_merge($books, $category_books);
                }
            }
            foreach ($books as $b) {
                $id = $b['id'];
            ?>
            <div class="good container">
                <img src="http://njnj.ru/pix/voc/book.gif" alt="book">
                <a href="<?php echo "book_page.php"."?id=$id" ?>" class="title"><?php echo $b['name'] ?></a>
                Author:
                <span class="author"><?php echo $b['author'] ?></span><br />
                Year:
                <span class="year"><?php echo $b['year'] ?></span><br />
                Category:
                <span class="category"><?php  echo $b['category']?></span><br />
                <span class="description"><?php echo $b['description'] ?></span> <br />
                <span class="price"><?php echo $b['price'] ?>$</span>
            </div>
            <?php } ?>
        </div>

</div></div>
</body>
</html>