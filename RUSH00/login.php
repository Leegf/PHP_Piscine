<?php
session_start();
if ($_SESSION["guest"] == "true")
    $_SESSION["flag"] = "true";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>XBookShop: categories</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body id="work">
<div class="cont_2">
	<div class="head">
		<header><h1><a href="index.php">XBookShop</a></h1>
			<div class="search"></div>
			<form action="cart_buy.php" class="cart" method="POST">
				<button type="submit" value="OK">
					<img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
				</button>
			</form>
		</header>
	</div>
</div>
<div class="log">
    <?php
    if ($_SESSION["error_login"]) {
        if ($_SESSION["error_login"] == "err_l")
            echo '<div class="error" style="color:darkred; margin-bottom: 10px;">Wrong login! Try again!</div>';
	    else if ($_SESSION["error_login"] == "err_p")
		    echo '<div class="error" style="color:darkred; margin-bottom: 10px;">Wrong password! Try again!</div>';
	    $_SESSION["error_login"] = "";
    }
    ?>
    <form action="login_action.php" method="post">
        Enter your login: <br /> <input type="text" name="login"> <br />
        Enter your password: <br /> <input type="password" name="passwd">
		<button type="submit" name="submit" value="OK" class="submit">
			LOGIN
		</button>
        <br />
		Don't have an account yet? <a href="create.php">Create one.</a>
    </form>
</div>
</body>
</html>
