<?php

include "user.php";

function get_all_cart_entries() {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "SELECT * from cart";
    $result = mysqli_query($connection, $query);

    $cart_entries = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $cart_entries[] = $row;
        }
    }
    mysqli_close($connection);
    return $cart_entries;
}

function get_cart_entries_by_user($user_login) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $user = get_user_by_login($user_login);
    $user_id = $user['id'];
    $query = "SELECT cart.id, cart.quan, cart.book_id, books.name, books.author, books.price FROM cart INNER JOIN books ON cart.book_id = books.id WHERE cart.user_id = '$user_id'";
    $result = mysqli_query($connection, $query);

    $cart_entries = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $cart_entries[] = $row;
        }
    }
    mysqli_close($connection);
    return $cart_entries;
}

function add_cart_entry($user_login, $book_id, $quan=1){
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $user = get_user_by_login($user_login);
    $user_id = $user['id'];
    $entry = mysqli_fetch_assoc(mysqli_query($connection,
        "SELECT * FROM cart WHERE (user_id='$user_id' AND book_id='$book_id')"));

    if ($entry) {
        $new_q = $entry['quan'] + $quan;
        $entry_id = $entry['id'];
        $query = "UPDATE cart SET quan='$new_q' WHERE id='$entry_id'";
    } else {
        $query = "INSERT INTO cart(user_id, book_id, quan) VALUES ('$user_id', '$book_id', '$quan')";
    }

    if (!mysqli_query($connection, $query))
        echo "Adding was not successful"."</br>".mysqli_error($connection)."</br>";

    mysqli_close($connection);
}

function delete_cart_entry($id) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "DELETE FROM cart WHERE id='$id'";

    if (!mysqli_query($connection, $query))
        echo "Deleting was not successful"."</br>".mysqli_error($connection)."</br>";

    mysqli_close($connection);
}

function delete_cart_entries_by_user($user_login) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $user = get_user_by_login($user_login);
    $user_id = $user['id'];
    $query = "DELETE FROM cart WHERE user_id='$user_id'";

    if (!mysqli_query($connection, $query))
        echo "Deleting was not successful"."</br>".mysqli_error($connection)."</br>";

    mysqli_close($connection);
}

?>