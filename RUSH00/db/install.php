<?php
include "user.php";

$servername = "localhost";
$username = "root";
$password = "671122";

$connection = mysqli_connect($servername, $username, $password);
if (!$connection) {
    die("Connection failed: " . mysqli_connect_error())."<br/>";
}

$dbname = "xbookshop";
$query = "CREATE DATABASE $dbname";
if (!mysqli_query($connection, $query)) {
    echo mysqli_error($connection)."<br/>";
}

if (!mysqli_select_db($connection, $dbname)) {
    echo mysqli_error($connection)."<br/>";
}


// create books table
$query = "CREATE TABLE books (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(100) NOT NULL,
author VARCHAR(100) NOT NULL,
category VARCHAR(100) NOT NULL,
year YEAR,
description TEXT,
price INT NOT NULL
)";

if (!mysqli_query($connection, $query)) {
    echo mysqli_error($connection)."<br/>";
}


// create users table
$query = "CREATE TABLE users (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
login VARCHAR(100) NOT NULL UNIQUE,
password TEXT NOT NULL,
role CHAR(10) NOT NULL,
email VARCHAR(100),
fullname VARCHAR(200)
)";

if (!mysqli_query($connection, $query)) {
    echo mysqli_error($connection)."<br/>";
}

$hash_pass = hash('whirlpool', "admin");
add_user('admin', $hash_pass, 'admin', "admin@gmail.com", "Admin");


// create cart table
$query = "CREATE TABLE cart (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
user_id INT NOT NULL,
book_id INT NOT NULL,
quan INT NOT NULL
)";

if (!mysqli_query($connection, $query)) {
    echo mysqli_error($connection)."<br/>";
}


//create more tables

$query = "CREATE TABLE orders(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
user_id INT(6) NOT NULL,
adress TEXT NOT NULL,
total_cost INT NOT NULL,
receiver VARCHAR(200),
status CHAR(100)
)";

if (!mysqli_query($connection, $query)) {
    echo mysqli_error($connection)."<br/>";
}

$query = "CREATE TABLE ordered_items(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
user_id INT(6) NOT NULL,
book_id INT(6) NOT NULL,
order_id INT NOT NULL,
quan INT NOT NULL
)";

if (!mysqli_query($connection, $query)) {
    echo mysqli_error($connection)."<br/>";
}

mysqli_close($connection);
?>