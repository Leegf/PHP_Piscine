<?php
include "db.php";

function get_all_books() {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "SELECT * from books";
    $result = mysqli_query($connection, $query);

    $books = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $books[] = $row;
        }
    }
    mysqli_close($connection);
    return $books;
}

function get_book_by_category($category, $limit=-1) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    if ($limit > 0) {
        $query = "SELECT * FROM books WHERE category='$category' LIMIT $limit";
    } else {
        $query = "SELECT * FROM books WHERE category='$category'";
    }
    $result = mysqli_query($connection, $query);

    $books = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $books[] = $row;
        }
    }
    mysqli_close($connection);
    return $books;
}

function get_book_by_id($id) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "SELECT * FROM books WHERE id='$id'";
    $result = mysqli_query($connection, $query);

    $book = mysqli_fetch_assoc($result);

    mysqli_close($connection);
    return $book;
}


function add_book($name, $author, $year, $category, $price, $description){
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "INSERT INTO books(name, author, year, category, price, description)
  VALUES ('$name', '$author', '$year', '$category', '$price', '$description')";

    if (!mysqli_query($connection, $query))
        echo "Adding was not successful"."</br>".mysqli_error($connection)."</br>";

    mysqli_close($connection);
}

function delete_book_by_id ($id) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "DELETE FROM books WHERE id='$id'";

    if (!mysqli_query($connection, $query))
        echo "Deleting was not successful"."</br>".mysqli_error($connection)."</br>";

    mysqli_close($connection);
}

?>