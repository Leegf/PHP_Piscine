<?php
include "book.php";

function auth($login, $pass) {
    // wrong login -- 0
    // wrong pass -- -1
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "SELECT * from users WHERE login='$login'";
    $user = mysqli_fetch_assoc(mysqli_query($connection, $query));

    if (!$user)
        return 0;
    if ($user['password'] === hash("whirlpool", $pass)) {
        return 1;
    } else {
        return -1;
    }
}

function get_all_users() {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "SELECT * from users";
    $result = mysqli_query($connection, $query);

    $users = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $users[] = $row;
        }
    }

    mysqli_close($connection);
    return $users;
}

function get_user_by_id($id) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "SELECT * FROM users WHERE id='$id'";
    $result = mysqli_query($connection, $query);

    $user = mysqli_fetch_assoc($result);

    mysqli_close($connection);
    return $user;
}

function get_user_by_login($login) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "SELECT * FROM users WHERE login='$login'";
    $result = mysqli_query($connection, $query);

    $user = mysqli_fetch_assoc($result);

    mysqli_close($connection);
    return $user;
}

function user_login_exists($login) {
    if (get_user_by_login($login))
        return true;
    return false;
}

function add_user($login, $pass, $role, $email, $fullname) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "INSERT INTO users(login, password, role, email, fullname)
  VALUES ('$login', '$pass', '$role', '$email', '$fullname')";

    if (!mysqli_query($connection, $query))
        echo "Adding was not successful"."</br>".mysqli_error($connection)."</br>";

    mysqli_close($connection);
}

function delete_user_by_id ($id) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $query = "DELETE FROM users WHERE id='$id'";

    if (!mysqli_query($connection, $query))
        echo "Deleting was not successful"."</br>".mysqli_error($connection)."</br>";

    mysqli_close($connection);
}

?>