<?php

include "cart.php";

function validate_order($login, $realname, $adress) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $user = get_user_by_login($login);
    $user_id = $user['id'];

    $query = "SELECT * FROM cart INNER JOIN books ON cart.book_id = books.id WHERE cart.user_id = '$user_id'";
    $result = mysqli_query($connection, $query);

    $cart_entries = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $cart_entries[] = $row;
        }
    }

    if (count($cart_entries) == 0)
        return false;

    $total_cost = 0;
    foreach ($cart_entries as $b) {
        $total_cost += $b['price'] * $b['quan'];
    }

    $query = "INSERT INTO orders(total_cost, user_id, status, receiver, adress) VALUES ('$total_cost', '$user_id', 'pending', '$realname', '$adress')";
    if (!mysqli_query($connection, $query))
        echo "Adding was not successful"."</br>".mysqli_error($connection)."</br>";

    $order_id = mysqli_insert_id($connection);

    for ($i = 0; $i < count($cart_entries); $i++) {
        $b = $cart_entries[$i];
        $cur_book_id = $b['book_id'];
        $cur_quan = $b['quan'];
        $query = "INSERT INTO ordered_items(user_id, book_id, order_id, quan) VALUES ('$user_id', '$cur_book_id', '$order_id', '$cur_quan')";
        if (!mysqli_query($connection, $query))
            echo "Adding was not successful"."</br>".mysqli_error($connection)."</br>";
    }

    mysqli_close($connection);
    delete_cart_entries_by_user($login);
    return true;
}

function get_all_orders(){
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $result = mysqli_query($connection, "SELECT * FROM orders WHERE status = 'pending'");
    $orders = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $orders[] = $row;
        }
    }

    $formatted_orders = array();
    foreach ($orders as $ord) {
        $ord_id = $ord['id'];
        $query = "SELECT * FROM ordered_items WHERE order_id = $ord_id";
        $result = mysqli_query($connection, $query);

        $ordered_books = array();
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $ord_book = get_book_by_id($row['book_id']);
                $ord_book['quan'] = $row['quan'];
                $ordered_books[] = $ord_book;
            }
        }
        $ord['ordered_books'] = $ordered_books;

        $ord['login'] = get_user_by_id($ord['user_id'])['login'];
        $formatted_orders[] = $ord;
    }

    mysqli_close($connection);
    return $formatted_orders;
}

function get_order_by_id($id) {
    $connection = receive_connection();
    if (!$connection) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $result = mysqli_query($connection, "SELECT * FROM orders WHERE status = 'pending' AND id = '$id'");
    $ord = mysqli_fetch_assoc($result);


    $ord_id = $ord['id'];
    $query = "SELECT * FROM ordered_items WHERE order_id = $ord_id";
    $result = mysqli_query($connection, $query);

    $ordered_books = array();
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $ord_book = get_book_by_id($row['book_id']);
            $ord_book['quan'] = $row['quan'];
            $ordered_books[] = $ord_book;
        }
    }
    $ord['ordered_books'] = $ordered_books;

    $ord['login'] = get_user_by_id($ord['user_id'])['login'];
    $formatted_orders = $ord;

    mysqli_close($connection);
    return $formatted_orders;
}