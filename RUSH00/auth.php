<?php
function auth($login, $passwd)
{
	if ($login == "" || $passwd == "") {
		return 0;
	}
	$arr2 = unserialize(file_get_contents("../private/data"));
	foreach ($arr2 as $elem) {
		if ($elem["login"] == $login) {
			if ($elem["passwd"] == hash("whirlpool", $passwd)) {
				return (1);
			}
			else
				return (-1);
		}
	}
	return (0);
}
?>