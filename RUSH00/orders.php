<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XBookShop</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body id="work">
<div class="cont_2">
    <div class="head">
        <header><h1><a href="index.php">XBookShop</a></h1>
            <div class="search"></div>
            <form action="cart_buy.php" class="cart" method="POST">
                <button type="submit" value="OK">
                    <img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
                </button>
            </form>
            <form action="login.php" class="login" method="POST">
				<?php
				if ($_SESSION["loggued_on_user"]) {
					echo '<button type="submit" value="OK" name="submit" disabled class="logout">' . 'Welcome, ' . $_SESSION['loggued_on_user'] . '
<ul class="additional_func hid_log">
<li class="elem">
<a href="logout.php">logout</a>
</li>';
					if ($_SESSION["loggued_on_user"] != "admin")
						echo '</ul>';
					else
						echo '
<li class="elem">
<a href="add_book.php">add book</a>
</li>
<li class="elem">
<a href="orders.php">manage orders</a>
</li>
<li class="elem">
<a href="users.php">manage users</a>
</li>
</ul>
 </button>';
				}
				else
					echo '<button type="submit" value="OK" name="submit">login</button>';
				?>
            </form>
        </header>
    </div>
</div>
<div class="container">
    <aside class="side_category">
    </aside>
    <div class="goods" id="order_goods">
        <strong>List of orders:</strong>
		<?php
		include "db/order.php";
		$orders = get_all_orders();
		foreach ($orders as $o) {
			$id = $o['id'];
			?>
            <div class="cart_good" id="cart_rew">
                <span class="order_num"><b>order:</b> <a href="<?php echo "order_page.php"."?id=$id"?>"><?php echo $id?></a></span>
                <span class="login_of_user"><b>ordered by:</b> <?php echo $o['login']?> </span>
                <span class="cost"><b>total cost:</b> <?php echo $o['total_cost']?> </span>
                <span class="xx"><a href="order_action.php"><img src="https://cdn2.iconfinder.com/data/icons/flat-style-svg-icons-part-1/512/apply_ok_check_yes_dialog-512.png" alt="cross" height="15px" width="15px"></a></span>
            </div>
			<?php
		}
		?>
    </div>
</div>
</body>
</html>

