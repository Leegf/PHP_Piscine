<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XBookShop</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body class="background_image">
<div class="cont">
    <div class="head">
    <header><h1><a href="index.php">XBookShop</a></h1>
        <div class="search"></div>
        <form action="cart_buy.php" class="cart" method="POST">
            <button type="submit" value="OK">
                <img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
            </button>
        </form>
        <form action="login.php" class="login" method="POST">
            <?php
            if ($_SESSION["loggued_on_user"]) {
	            echo '<button type="submit" value="OK" name="submit" disabled class="logout">' . 'Welcome, ' . $_SESSION['loggued_on_user'] . '
<ul class="additional_func hid_log">
<li class="elem">
<a href="logout.php">logout</a>
</li>';
	            if ($_SESSION["user_role"] != "admin")
		            echo '</ul>';
	            else
		            echo '
<li class="elem">
<a href="add_book.php">add book</a>
</li>
<li class="elem">
<a href="orders.php">manage orders</a>
</li>
<li class="elem">
<a href="users.php">manage users</a>
</li>
</ul>
 </button>';
            }
            else
	            echo '<button type="submit" value="OK" name="submit">login</button>';
            ?>
        </form>
    </header>
<nav>
    <?php include "db/book.php"?>
    <ul class="nav">
        <li class="hover_li"><a href="" class="el first">Horror</a>
            <ul class="hidden">
                <?php
                $books = get_book_by_category('horror', 5);
                foreach ($books as $b) {
                    $id = $b['id'];
                    ?>

                    <li><a href="<?php echo "book_page.php"."?id=$id" ?>"><?php echo $b['name'] ?></a></li>
                <?php } ?>
            </ul>
        </li>
        <li class="hover_li"><a href="" class="el">Thriller</a>
        <ul class="hidden">
            <?php
            $books = get_book_by_category('thriller', 5);
            foreach ($books as $b) {
                $id = $b['id'];
                ?>

                <li><a href="<?php echo "book_page.php"."?id=$id" ?>"><?php echo $b['name'] ?></a></li>
            <?php } ?>
        </ul>
        </li>
        <li class="hover_li"><a href="" class="el">Fantasy</a>
        <ul class="hidden">
            <?php
            $books = get_book_by_category('fantasy', 5);
            foreach ($books as $b) {
                $id = $b['id'];
                ?>

                <li><a href="<?php echo "book_page.php"."?id=$id" ?>"><?php echo $b['name'] ?></a></li>
            <?php } ?>
        </ul>
        </li>
        <li class="hover_li"><a href="" class="el">Science Fiction</a>
        <ul class="hidden">
            <?php
            $books = get_book_by_category('science_fiction', 5);
            foreach ($books as $b) {
                $id = $b['id'];
                ?>

                <li><a href="<?php echo "book_page.php"."?id=$id" ?>"><?php echo $b['name'] ?></a></li>
            <?php } ?>
        </ul>
        </li>
        <li class="hover_li"><a href="" class="el">Light novel</a>
        <ul class="hidden">
            <?php
            $books = get_book_by_category('light_novel', 5);
            foreach ($books as $b) {
                $id = $b['id'];
                ?>

                <li><a href="<?php echo "book_page.php"."?id=$id" ?>"><?php echo $b['name'] ?></a></li>
            <?php } ?>
        </ul></li>
        <li class="hover_li"><a href="" class="el">Novel</a>
        <ul class="hidden">
            <?php
            $books = get_book_by_category('novel', 5);
            foreach ($books as $b) {
                $id = $b['id'];
                ?>

                <li><a href="<?php echo "book_page.php"."?id=$id" ?>"><?php echo $b['name'] ?></a></li>
            <?php } ?>
        </ul>
        </li>
        <li class="hover_li"><a href="" class="el">Detective</a>
        <ul class="hidden">
            <?php
            $books = get_book_by_category('detective', 5);
            foreach ($books as $b) {
                $id = $b['id'];
                ?>

                <li><a href="<?php echo "book_page.php"."?id=$id" ?>"><?php echo $b['name'] ?></a></li>
            <?php } ?>
        </ul>
        </li>
</ul>
</nav>
    </div>
<a href="shop.php"><H1 id="mainpage_info">GO SHOPPING</H1></a>
</div>
</body>
</html>