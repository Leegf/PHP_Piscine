<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XBookShop</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body id="work">
<div class="cont_2">
    <div class="head">
    <header><h1><a href="index.php">XBookShop</a></h1>
        <div class="search"></div>
        <form action="cart_buy.php" class="cart" method="POST">
            <button type="submit" value="OK">
                <img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
            </button>
        </form>
        <form action="login.php" class="login" method="POST">
            <?php
            if ($_SESSION["loggued_on_user"]) {
	            echo '<button type="submit" value="OK" name="submit" disabled class="logout">' . 'Welcome, ' . $_SESSION['loggued_on_user'] . '
<ul class="additional_func hid_log">
<li class="elem">
<a href="logout.php">logout</a>
</li>';
	            if ($_SESSION["loggued_on_user"] != "admin")
		            echo '</ul>';
	            else
		            echo '
<li class="elem">
<a href="add_book.php">add book</a>
</li>
<li class="elem">
<a href="orders.php">manage orders</a>
</li>
<li class="elem">
<a href="users.php">manage users</a>
</li>
</ul>
 </button>';
            }
            else
	            echo '<button type="submit" value="OK" name="submit">login</button>';
            ?>
        </form>
    </header>
    </div>
</div>
<div class="log">
    <form action="add_book_action.php" method="post">
        Enter title of the book: <br /> <input type="text" name="name" required> <br />
        Enter price of the book: <br /> <input type="number" name="price" min="0" required> <br />
        Choose a category:
        <select name="category">
            <option value="horror">Horror</option>
            <option value="thriller">Thriller</option>
            <option value="fantasy">Fantasy</option>
            <option value="science_fiction">Science Fiction</option>
            <option value="light_novel">Light Novel</option>
            <option value="detective">Detective</option>
        </select> <br /> <br />
        Enter year it was published: <br /> <input type="number" min="1000"gtgt name="year" required> <br />
        Enter author's name: <br /> <input type="text" name="author" required>
        Enter the description: <br /> <textarea rows="25" cols="55" name="description" required></textarea>
        <button type="submit" name="submit" value="OK" class="submit">
            ADD
        </button>
        <br />
    </form>
</div>
</body>
</html>
