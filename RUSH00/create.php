<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XBookShop: categories</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body id="work">
<div class="cont_2">
    <div class="head">
        <header><h1><a href="index.php">XBookShop</a></h1>
            <div class="search"></div>
            <form action="cart_buy.php" class="cart" method="POST">
                <button type="submit" value="OK">
                    <img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
                </button>
            </form>
        </header>
    </div>
</div>
<div class="log">
	<?php
	if ($_SESSION["error_register"]) {
		if ($_SESSION["error_register"] == "err1")
			echo '<div class="error" style="color:darkred; margin-bottom: 10px;">Passwords does not match, try again.</div>';
		else if ($_SESSION["error_register"] == "err")
			echo '<div class="error" style="color:darkred; margin-bottom: 10px;">Someone has already taken such login!</div>';
		else if ($_SESSION["error_register"] == "err2")
			echo '<div class="error" style="color:darkred; margin-bottom: 10px;">Your password is too short! Try better.</div>';
		$_SESSION["error_register"] = "";
	}
	?>
    <form action="create_action.php" method="post">
        Enter your email: <br /> <input type="email" name="email" required> <br />
        Enter your login: <br /> <input type="text" name="login" required> <br />
        Enter your password: <br /> <input type="password" name="passwd" required>
        Repeat your password: <br /> <input type="password" name="passwd2" required>
        <button type="submit" name="submit" value="OK" class="submit">
            REGISTER
        </button>
        <br />
    </form>
</div>
</body>
</html>
