<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>XBookShop: categories</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body id="return_button">
<div class="cont_2">
    <div class="head">
        <header><h1><a href="index.php">XBookShop</a></h1>
            <div class="search"></div>
            <form action="cart_buy.php" class="cart" method="POST">
                <button type="submit" value="OK">
                    <img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
                </button>
            </form>
            <form action="login.php" class="login" method="POST">
			    <?php
			    if ($_SESSION["loggued_on_user"]) {
				    echo '<button type="submit" value="OK" name="submit" disabled class="logout">' . 'Welcome, ' . $_SESSION['loggued_on_user'] . '
<ul class="additional_func hid_log">
<li class="elem">
<a href="logout.php">logout</a>
</li>';
				    if ($_SESSION["loggued_on_user"] != "admin")
					    echo '</ul>';
				    else
					    echo '
<li class="elem">
<a href="add_book.php">add book</a>
</li>
<li class="elem">
<a href="orders.php">manage orders</a>
</li>
<li class="elem">
<a href="users.php">manage users</a>
</li>
</ul>
 </button>';
			    }
			    else
				    echo '<button type="submit" value="OK" name="submit">login</button>';
			    ?>
            </form>
        </header>
    </div>
</div>
<?php
include "db/book.php";

$id = $_GET['id'];
$book = get_book_by_id($id);
// todo return 404 if !$book
?>
<div class="container" id="book_page">
    <div class="img"><img src="http://njnj.ru/pix/voc/book.gif" alt="book">
        <div class="description_item">
            <b class="title"><?php echo $book['name'] ?></b>
            <i class="author"><?php echo $book['author'] ?></i> <br />
            <i class="year"><?php echo $book['year'] ?></i>
            <i class="year"><?php echo $book['category'] ?></i> <br />
            <i class="price"><?php echo $book['price'] ?>$</i> <br />
            <span class="desc"><?php echo $book['description'] ?></span>
            <div class="buttons">
            <form action="buy.php" method="post" class="coolform">
                <input type="hidden" name="book_id" value="<?php echo $book['id']?>">
               <button type="submit" name="submit" value="OK" class="buy_book">
                   BUY
               </button>
            </form>
            <form action="modif" method="post" class="coolform" id="modify_form">
                <button type="submit" name="submit" value="OK" class="buy_book">
                    Modify
                </button>
            </form>
                <?php if ($_SESSION["user_role"] == "admin") echo '
                <form action="delete_book.php" method="post" class="coolform" id="delete_form">
                    <input type="hidden" name="id" value='.$book['id'].'>
                    <button type="submit" name="submit" value="OK" class="buy_book">
                        Delete
                    </button>'; ?>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="return">
    <a href="shop.php"><img src="https://cdn1.iconfinder.com/data/icons/flatastic-8/256/go-back-512.png" alt="return_button"></a>
</div>
</body>
</html>
