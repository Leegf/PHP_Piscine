<?php
session_start();

include "db/user.php";

if ($_POST["passwd"] != $_POST["passwd2"]) {
	$_SESSION["error_register"]="err1";
	header("Location: create.php");
	exit;
}

if (user_login_exists($_POST["login"])) {
	$_SESSION["error_register"]="err";
	header("Location: create.php");
	exit;
}

if (strlen($_POST["passwd"]) < 8)
{
	$_SESSION["error_register"]="err2";
	header("Location: create.php");
	exit;
}

//$arr2[] = ["login"=>$_POST["login"], "email"=>$_POST["email"], "passwd"=>hash("whirlpool", $_POST["passwd"])];
//file_put_contents("../private/data", serialize($arr2));

add_user($_POST["login"], hash("whirlpool", $_POST["passwd"]), 'user', $_POST["email"], "");

header("Location: login.php");
?>
