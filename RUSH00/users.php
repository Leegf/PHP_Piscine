<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>XBookShop</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body id="work">
<div class="cont_2">
	<div class="head">
		<header><h1><a href="index.php">XBookShop</a></h1>
			<div class="search"></div>
			<form action="cart_buy.php" class="cart" method="POST">
				<button type="submit" value="OK">
					<img src="https://findicons.com/files/icons/1700/2d/512/cart.png" alt="cart" class="cart_image">
				</button>
			</form>
			<form action="login.php" class="login" method="POST">
				<?php
				if ($_SESSION["loggued_on_user"]) {
					echo '<button type="submit" value="OK" name="submit" disabled class="logout">' . 'Welcome, ' . $_SESSION['loggued_on_user'] . '
<ul class="additional_func hid_log">
<li class="elem">
<a href="logout.php">logout</a>
</li>';
					if ($_SESSION["loggued_on_user"] != "admin")
						echo '</ul>';
					else
						echo '
<li class="elem">
<a href="add_book.php">add book</a>
</li>
<li class="elem">
<a href="orders.php">manage orders</a>
</li>
<li class="elem">
<a href="users.php">manage users</a>
</li>
</ul>
 </button>';
				}
				else
					echo '<button type="submit" value="OK" name="submit">login</button>';
				?>
			</form>
		</header>
	</div>
</div>
<div class="container">
    <aside class="side_category">
    </aside>
    <div class="goods" id="order_goods">
        <strong>List of users:</strong>
		<?php
		include "db/user.php";
		$users = get_all_users();
		foreach ($users as $user) {
		    $id = $user['id'];
			?>
            <div class="cart_good" id="cart_rew">
                <span class="name_of_user"><b>login:</b> <?php echo $user['login'];?> </span>
                <span class="email_of_user"><b>email:</b> <?php echo $user['email'];?></span>
                <span class="xx">
                    <a href="<?php echo "delete_user.php"."?id=$id"?>"><img src="https://www.pngarts.com/files/1/X-Shape-Free-PNG-Image.png" alt="cross" height="15px" width="15px"></a>
                </span>
            </div>
			<?php
		}
		?>
    </div>
</div>
</body>
</html>

