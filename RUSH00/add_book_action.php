<?php
session_start();

include "db/book.php";

header("Content-type:text/html");
if ($_SESSION["loggued_on_user"] && $_SESSION["loggued_on_user"] != "") {
    add_book($_POST["name"], $_POST["author"], $_POST["year"], $_POST["category"], $_POST["price"], $_POST["description"]);
    header("location: shop.php");
}