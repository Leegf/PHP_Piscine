<?php
include ("db/user.php");

session_start();
header("Content-type:text/html");
if (auth($_POST["login"], $_POST["passwd"]) == 0) {
	$_SESSION["loggued_on_user"] = "";
	$_SESSION["error_login"] = "err_l";
	header("Location: login.php");
	echo "1";
}
else if (auth($_POST["login"], $_POST["passwd"]) == -1) {
	$_SESSION["loggued_on_user"] = "";
	$_SESSION["error_login"] = "err_p";
	header("Location: login.php");
	echo "2";
}

else {
	$user = get_user_by_login($_POST["login"]);
	$_SESSION["loggued_on_user"] = $_POST["login"];
	$_SESSION["user_role"] = $user["role"];
	header("Location: index.php");
}
