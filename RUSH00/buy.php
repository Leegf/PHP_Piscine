<?php
include "db/cart.php";

session_start();

if ($_SESSION["loggued_on_user"] && $_SESSION["loggued_on_user"] != "") {
	if ($_SESSION["guest"] == "true") {
		foreach ($_SESSION["books"] as $elem) {
			add_cart_entry($_SESSION["loggued_on_user"], $elem);
		}
		add_cart_entry($_SESSION["loggued_on_user"], $_POST["book_id"]);
		$_SESSION["books"] = "";
		$_SESSION["guest"] = "";
		if ($_SESSION["from_cart"] == "true") {
			$_SESSION["from_cart"] = "";
			$_SESSION["flag"] = "";
			header("location: cart_buy.php");
			exit;
		}
		$_SESSION["flag"] = "";
		header("location: shop.php");
		exit;
	}
    add_cart_entry($_SESSION["loggued_on_user"], $_POST["book_id"]);
    header("location: shop.php");
	exit;
}
else
{
	if ($_SESSION["guest"] == "true") {
		array_push($_SESSION["books"], $_POST["book_id"]);
	}
	else {
		$_SESSION["guest"] = "true";
		$_SESSION["books"] = array();
		array_push($_SESSION["books"], $_POST["book_id"]);
	}
	header("location: shop.php");
}
?>