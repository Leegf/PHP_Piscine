<?php

class Color
{
	public $red = 0;
	public $green = 0;
	public $blue = 0;
	public $rgb = 0;
	private $tmp = 0;
	static public $verbose = false;

	public static function doc() {
		$str = file_get_contents("Color.doc.txt");
		echo $str;
	}
	public function __construct(array $kwargs) {
		if (array_key_exists("rgb", $kwargs)) {
			$this->tmp = (substr(bin2hex(pack("I",
				$kwargs["rgb"])),0, 6));
			$this->_split_to_colors($this->tmp);
		}
		else {
			if (array_key_exists("red", $kwargs))
				$this->red = intval($kwargs["red"]);
			if (array_key_exists("green", $kwargs))
				$this->green = intval($kwargs["green"]);
			if (array_key_exists("blue", $kwargs))
				$this->blue = intval($kwargs["blue"]);
		}
		if (self::$verbose) {
			echo $this->__toString();
			print(" constructed.\n");
		}
		return ;
	}
	public function __toString()
	{
		return (sprintf ("Color( red: %3d, green: %3d, blue: %3d )",
		$this->red,
		$this->green, $this->blue));
	}

	public function __destruct() {
		if (self::$verbose) {
			echo $this->__toString();
			print(" destructed.\n");
		}
		return ;
	}
	private function _split_to_colors($str)
	{
		$this->red = intval(substr($str, 4, 2 ), 16);
		$this->green = intval(substr($str, 2, 2 ), 16);
		$this->blue = intval(substr($str, 0, 2 ), 16);
	}
	private function _debug()
	{
		echo "red = " . $this->red . "\n";
		echo "green = ". $this->green . "\n";
		echo "blue = ". $this->blue . "\n";
	}
	public function add($Color)
	{
		return (new Color(array('red' => $this->red + $Color->red, 'green' =>
			$this->green + $Color->green, 'blue' => $this->blue + $Color->blue)));
	}
	public function sub($Color)
	{
		return (new Color(array('red' => $this->red - $Color->red, 'green' =>
			$this->green - $Color->green, 'blue' => $this->blue - $Color->blue)));
	}
	public function mult($f)
	{
		return (new Color(array('red' => $this->red * $f, 'green' =>
			$this->green * $f, 'blue' => $this->blue * $f)));
	}
}
?>
