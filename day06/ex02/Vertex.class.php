<?php

require_once('Color.class.php');

class Vertex
{
	private $_x = 0;
	private $_y = 0;
	private $_z = 0;
	private $_w = 1.0;
	private $_Color;
	static public $verbose = false;

	public static function doc() {
		$str = file_get_contents("Vertex.doc.txt");
		echo $str;
	}
	public function __construct(array $kwargs) {
		if (array_key_exists('x', $kwargs)) {
			$this->_x = $kwargs['x'];
		}
		else
			return;
		if (array_key_exists('y', $kwargs)) {
			$this->_y = $kwargs['y'];
		}
		else
			return ;
		if (array_key_exists('z', $kwargs)) {
			$this->_z = $kwargs['z'];
		}
		else
			return ;
		if (array_key_exists('w', $kwargs)) {
			$this->_w = $kwargs['w'];
		}
		if (array_key_exists('color', $kwargs)) {
			$this->_Color = $kwargs['color'];
		}
		else {
			$this->_Color = new Color(array( 'red' => 255, 'green' => 255, 'blue' => 255));
		}
		if (self::$verbose) {
			echo $this->__toString();
			print(" constructed\n");
		}
		return;
	}
	public function __destruct() {
		if (self::$verbose) {
			echo $this->__toString();
			print(" destructed\n");
		}
		return;
	}
	public function __get($_x) {
		 return ($this->$_x);
	}
	public function __set($_x, $val) {
		$this->$_x = $val;
	}
	public function __toString()
	{
		 $str = sprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f )", $this->_x, $this->_y, $this->_z, $this->_w);
		 if (self::$verbose) {
		 	$str = substr($str, 0, -2);
		 	$str = $str . ", " . $this->_Color->__toString() . " )";
		 }
		 return ($str);
	}
}
