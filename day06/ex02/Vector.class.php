<?php

require_once('Color.class.php');
require_once('Vertex.class.php');

class Vector
{
	private $_x = 0;
	private $_y = 0;
	private $_z = 0;
	private $_w = 0.0;
	static public $verbose = false;

	public static function doc() {
		$str = file_get_contents("Vector.doc.txt");
		echo $str;
	}
	public function __construct(array $kwargs) {
		if (array_key_exists('dest', $kwargs)) {
			if (array_key_exists('orig', $kwargs)) {
				$this->_x = $kwargs['dest']->_x - $kwargs['orig']->_x;
				$this->_y = $kwargs['dest']->_y - $kwargs['orig']->_y;
				$this->_z = $kwargs['dest']->_z - $kwargs['orig']->_z;
				$this->_w = $kwargs['dest']->_w - $kwargs['orig']->_w;
			}
			else {
				$tmp = new Vertex(array("x" => 0, "y" => 0, "z" => 0, "w" => 1));
				$this->_x = $kwargs['dest']->_x - $tmp->_x;
				$this->_y = $kwargs['dest']->_y - $tmp->_y;
				$this->_z = $kwargs['dest']->_z - $tmp->_z;
				$this->_w = $kwargs['dest']->_w - $tmp->_w;
			}
		}
		else
			return;
		if (self::$verbose) {
			echo $this->__toString();
			print(" constructed\n");
			return ;
		}
	}
	public function __destruct() {
		if (self::$verbose) {
			echo $this->__toString();
			print(" destructed\n");
		}
		return;
	}
	public function __get($_x) {
		return ($this->$_x);
	}
	public function __toString()
	{
		$str = sprintf("Vector( x: %.2f, y: %.2f, z:%.2f, w:%.2f )", $this->_x, $this->_y, $this->_z, $this->_w);
		return $str;
	}
	public function magnitude()
	{
		return sqrt(($this->_x * $this->_x) + ($this->_y * $this->_y) + ($this->_z * $this->_z));
	}
	public function normalize()
	{
		$vect = new Vertex(array('x' => $this->_x / $this->magnitude(), 'y' => $this->_y / $this->magnitude(), 'z' => $this->_z / $this->magnitude()));
		return (new Vector(array('dest' => $vect)));
	}
	public function add(Vector $rhs)
	{
		$vect = new Vertex(array('x' => $this->_x + $rhs->_x, 'y' => $this->_y + $rhs->_y, 'z' => $this->_z + $rhs->_z));
		return (new Vector(array('dest' => $vect)));
	}
	public function sub(Vector $rhs)
	{
		$vect = new Vertex(array('x' => $this->_x - $rhs->_x, 'y' => $this->_y - $rhs->_y, 'z' => $this->_z - $rhs->_z));
		return (new Vector(array('dest' => $vect)));
	}
	public function opposite()
	{
		$vect = new Vertex(array('x' => $this->_x, 'y' => $this->_y, -$this->_z));
		return (new Vector(array('dest' => $vect)));
	}
	public function scalarProduct($k) {
		$vect = new Vertex(array('x' => $this->_x * $k, 'y' => $this->_y * $k, 'z' => $this->_z * $k));
		return (new Vector(array('dest' => $vect)));
	}
	public function dotProduct(Vector $rhs) {
		$tmp = $this->_x * $rhs->_x + $this->_y * $rhs ->_y + $this->_z * $rhs->_z;
		return ($tmp);
	}
	public function cos(Vector $rhs) {
		return ($this->dotProduct($rhs)/($this->magnitude() * $rhs->magnitude()));
	}
	public function crossProduct(Vector $rhs)
	{
		$vect = new Vertex(array('x' => $this->_y * $rhs->_z - $this->_z * $rhs->_y, 'y' => $this->_z * $rhs->_x - $this->_x * $rhs->_z, 'z' => $this->_x * $rhs->_y - $this->_y * $rhs->_x));
		return (new Vector(array('dest' => $vect)));
	}
}
